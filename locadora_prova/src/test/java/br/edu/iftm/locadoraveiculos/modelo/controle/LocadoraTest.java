package br.edu.iftm.locadoraveiculos.modelo.controle;

import br.edu.iftm.locadoraveiculos.modelo.Acessorio;
import br.edu.iftm.locadoraveiculos.modelo.Carro;
import br.edu.iftm.locadoraveiculos.modelo.Fabricante;
import br.edu.iftm.locadoraveiculos.modelo.ModeloCarro;
import br.edu.iftm.locadoraveiculos.modelo.dao.AcessorioDAO;
import br.edu.iftm.locadoraveiculos.modelo.dao.CarroDAO;
import br.edu.iftm.locadoraveiculos.modelo.dao.DAOFactory;
import br.edu.iftm.locadoraveiculos.modelo.dao.FabricanteDAO;
import br.edu.iftm.locadoraveiculos.modelo.dao.ModeloDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aluno
 */
public class LocadoraTest {
    
    public LocadoraTest() {
    }

    
    
    @org.junit.Test
    public void testCadastro(){
        
        Carro carro = new Carro();
        
        assert carro != null;
    }
    
    @org.junit.Test
    public void testAtributos(){
        
        Carro carro = new Carro();
        List<Acessorio> acessorios = new ArrayList<>();
        ModeloCarro modelo = new ModeloCarro();
        Acessorio acessorio = new Acessorio();
        Fabricante fabricante = new Fabricante();
        
        fabricante.setCodigo(1);
        fabricante.setNome("fiat");
        
        modelo.setCodigo(4);
        modelo.setDescricao("Descrição");
        modelo.setFabricante(fabricante);
   
        acessorio.setCodigo(1);
        acessorio.setDescricao("preto");
        acessorios.add(acessorio);
        acessorio.setCodigo(2);
        acessorio.setDescricao("branco");
        acessorios.add(acessorio);

        carro.setPlaca("placa");
        carro.setCor("cor");
        carro.setChassi("chassi");
        carro.setValorDiaria(Double.parseDouble("99"));
        carro.setModelo(modelo);
        carro.setAcessorios(acessorios);
        
        assert carro != null;
        assert carro.getAcessorios() != null;
        assert carro.getModelo() != null;
        assert carro.getModelo().getFabricante() != null;
             
    }
    
    
}
